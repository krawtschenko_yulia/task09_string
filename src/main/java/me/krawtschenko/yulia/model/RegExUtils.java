package me.krawtschenko.yulia.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExUtils {
     public static boolean checkSentence(String input) {
        Pattern sentence = Pattern.compile("^[A-Z].+\\.$");
        Matcher matcher = sentence.matcher(input);

        return matcher.matches();
    }

    public static String[] splitOnRegEx(String input, Pattern divisor) {
        return divisor.split(input);
    }

    public static String vowelsToUnderscore(String input) {
        return input.replaceAll("[AOEUIaoeui]", "_");
    }

    public static String whitespaceToBlank(String input) {
        return input.replaceAll("\t| {2,}", " ");
    }
}
