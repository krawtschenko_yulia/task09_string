package me.krawtschenko.yulia.model;

import java.util.Comparator;

public class FirstConsonantWordComparator implements Comparator<Word> {
    @Override
    public int compare(Word one, Word two) {
        String first = stripLeadingVowels(one);
        String second = stripLeadingVowels(two);

        return first.compareTo(second);
    }

    private String stripLeadingVowels(Word word) {
        String string = word.toString();

        return string.replaceAll("^[aoeui]+", "");
    }
}
