package me.krawtschenko.yulia.model;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Text {
    HashMap<Word, Integer> words;
    String string;

    Text(String text) {
        string = text;
        parseWords(text);
    }

    Text(HashMap<Word, Integer> words) {
        this.words = words;
    }

    public TreeSet<Word> sortWords(Comparator<Word> comparator) {
        TreeSet<Word> sorted = new TreeSet<>(comparator);

        sorted.addAll(words.keySet());

        return sorted;
    }

    public TreeMap<Character, TreeSet<Word>> groupWordsByLetter() {
        TreeMap<Character, TreeSet<Word>> grouped = new TreeMap<>();

        for (Word word : words.keySet()) {
            char firstLetter = word.getFirstLetter();

            if (!grouped.containsKey(firstLetter)) {
                grouped.put(firstLetter, new TreeSet<>());
            }
            grouped.get(firstLetter).add(word);
        }
        return grouped;
    }

    Set<Word> filterWords(WordFilter filter) {
        return words.keySet().stream()
                .filter(filter::filter)
                .collect(Collectors.toSet());
    }

    private void parseWords(String text) {
        words = new HashMap<>();

        Pattern word = Pattern.compile("\\w+");
        Matcher wordMatcher = word.matcher(text);
        Word nextWord = null;

        while (wordMatcher.find()) {
            nextWord = new Word(wordMatcher.group());
            if (words.containsKey(nextWord)) {
                int count = words.get(nextWord) + 1;
                words.put(nextWord, count);
            } else {
                words.put(nextWord, 1);
            }
        }
    }
}
