package me.krawtschenko.yulia.view;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

public class ApplicationView<T> extends View{
    ResourceBundle bundle;

    public ApplicationView() {
        super();

        super.printMessage("Choose language:");
        String language = super.readLine();
        Locale lo;
        if (isValidLocale(language)) {
            lo = new Locale(language);
        } else {
            lo = new Locale("en");
        }

        bundle = ResourceBundle.getBundle("Application", lo);

        MenuItem quit = new MenuItem(MenuOption.QUIT, bundle.getString("QUIT"));
        menu.put(MenuOption.QUIT, quit);

        MenuItem manipulateStrings = new MenuItem(MenuOption.REGEX_ON_STRINGS,
                bundle.getString("STRINGS"));
        menu.put(MenuOption.REGEX_ON_STRINGS, manipulateStrings);

        MenuItem manipulateWords = new MenuItem(MenuOption.REGEX_ON_WORDS,
                bundle.getString("WORDS"));
        menu.put(MenuOption.REGEX_ON_WORDS, manipulateWords);

        MenuItem manipulateSentences = new MenuItem(MenuOption.REGEX_ON_SENTENCES,
                bundle.getString("SENTENCES"));
        menu.put(MenuOption.REGEX_ON_SENTENCES, manipulateSentences);

        MenuItem manipulateText = new MenuItem(MenuOption.REGEX_ON_TEXT,
                bundle.getString("TEXT"));
        menu.put(MenuOption.REGEX_ON_TEXT, manipulateText);
    }

    @Override
    public void printMessage(String message) {
        if (bundle.containsKey(message)) {
            super.printMessage(bundle.getString(message));
        } else {
            super.printMessage(message);
        }
    }

    public int readInt() {
        return input.nextInt();
    }

    public double readDouble() {
        return input.nextDouble();
    }

    public void printMap(Map<?, ?> map) {
        map.forEach((key, value) -> {
            System.out.println(key + ": " + value);
        });
    }

    public void printSet(Set<?> set) {
        set.forEach(System.out::println);
    }

    public void printArray(T[] array) {
        for (T element : array) {
            System.out.println(element);
        }
    }

    public void printGroupedWords(Map<?, Set<?>> map) {
        map.forEach(((key, valueSet) -> {
            System.out.print("\t");
            valueSet.forEach(System.out::println);
        }));
    }

    @Override
    public void displayMenu() {
        super.printMessage(bundle.getString("MENU"));
        super.displayMenu();
    }

    private boolean isValidLocale(String string) {
        Locale[] locales = Locale.getAvailableLocales();
        for (Locale locale : locales) {
            if (string.equals(locale.toString())) {
                return true;
            }
        }
        return false;
    }
}
