package me.krawtschenko.yulia.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence extends Text {
    private String sentence;

    public Sentence(String sentence) {
        super(sentence);
        this.sentence = sentence;
    }

    public boolean hasWordInCommon(Sentence other) {
        boolean answer = false;

        for (Word word : words.keySet()) {
            if (other.words.containsKey(word)) {
                answer = true;
                break;
            }
        };

        return answer;
    }

    public Word findWordInCommon(Sentence other) {
        for (Word word : words.keySet()) {
            if (other.words.containsKey(word)) {
                return word;
            }
        }
        return null;
    }

    public ArrayList<Word> getWordsInCommon(Sentence other) {
        ArrayList<Word> commonWords = new ArrayList<>();

        for (Word word : words.keySet()) {
            if (other.words.containsKey(word)) {
                commonWords.add(word);
            }
        }
        return commonWords;
    }

    public ArrayList<Word> getWordsInCommon(ArrayList<Word> shortListed) {
        ArrayList<Word> commonWords = new ArrayList<>();

        for (Word word : shortListed) {
            if (words.containsKey(word)) {
                commonWords.add(word);
            }
        }
        return commonWords;
    }

    public boolean containsWord(Word word) {
        return words.containsKey(word);
    }

    public boolean isInterrogative() {
        return sentence.matches(".+\\?$");
    }

    public HashSet<Word> getWordsOfLength(int length) {
        HashSet<Word> lengthy = new HashSet<>();

        for (Word word : words.keySet()) {
            if (word.getLength() == length) {
                lengthy.add(word);
            }
        }
        return lengthy;
    }

    public Word getLongestWord() {
        Word longest = null;

        for (Word word : words.keySet()) {
            if (longest == null) {
                longest = word;
            } else if (longest.getLength() < word.getLength()) {
                longest = word;
            }
        }
        return longest;
    }

    public String getFirstWordThatStartsWithVowel(Word replacement) {
        Pattern startsWithVowel = Pattern.compile("\\b[AOEUIaoeui].*");
        Matcher startsWithVowelMatcher = startsWithVowel.matcher(sentence);

        // startsWithVowelMatcher.find();
        return startsWithVowelMatcher.group();
    }

    public String switchWords(String one, String two) {
        if (one.equals(two)) {
            return sentence;
        }

        String[] splitAtFirst = sentence.split(one);
        String[] splitAtSecond = sentence.split(two);

        if (splitAtFirst[0].length() < splitAtSecond[0].length()) {
            String remainder = join(splitAtFirst, 1, splitAtFirst.length);
            remainder = remainder.replaceFirst(two, one);

            return splitAtFirst[0] + two + remainder;
        } else {
            String remainder = join(splitAtSecond, 1, splitAtSecond.length);
            remainder = remainder.replaceFirst(one, two);

            return splitAtSecond[0] + one + remainder;
        }
    }

    public int getNumberOfWordOccurrences(Word word) {
        return words.getOrDefault(word, 0);
    }

    private String join(String[] array, int start, int end) {
        StringBuilder stringToBe = new StringBuilder();

        for (int i = start; i < end; i++) {
            stringToBe.append(array[i]);
        }

        return stringToBe.toString();
    }

}
