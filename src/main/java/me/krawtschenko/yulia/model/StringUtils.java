package me.krawtschenko.yulia.model;

public class StringUtils {
    public static String concatenate(Object... input) {
        StringBuilder stringToBe = new StringBuilder();

        for (Object any : input) {
            stringToBe.append(any.toString());
        }

        return stringToBe.toString();
    }
}
