package me.krawtschenko.yulia;

import me.krawtschenko.yulia.controller.ApplicationController;

public class Application {
    public static void main(String[] args) {
        new ApplicationController().startMenu();
    }
}