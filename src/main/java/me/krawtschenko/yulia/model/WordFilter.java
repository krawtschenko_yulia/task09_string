package me.krawtschenko.yulia.model;

public interface WordFilter {
    boolean filter(Word word);
}