package me.krawtschenko.yulia.model;

import java.util.Comparator;
import java.util.stream.IntStream;

public class LetterOccurrencesWordComparator implements Comparator<Word> {
    private char letter;

    public LetterOccurrencesWordComparator(final char letter) {
        this.letter = letter;
    }

    @Override
    public int compare(Word one, Word two) {
        int inFirst = countLetterOccurrences(letter, one);
        int inSecond = countLetterOccurrences(letter, two);

        return inFirst - inSecond != 0
                ? inFirst - inSecond
                : one.compareTo(two);
    }

    private int countLetterOccurrences(char letter, Word word) {
        IntStream letters = word.toString().chars();
        long numberOccurrences = letters.filter(ch -> ch == letter).count();

        return Math.toIntExact(numberOccurrences);
    }
}
