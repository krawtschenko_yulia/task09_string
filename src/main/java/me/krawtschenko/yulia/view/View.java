package me.krawtschenko.yulia.view;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.TreeMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    Scanner input;
    TreeMap<MenuOption, MenuItem> menu;

    View() {
        OptionKeyComparator keyComparator= new OptionKeyComparator();

        input = new Scanner(System.in, StandardCharsets.UTF_8);
        menu = new TreeMap<>(keyComparator);
    }

    public void displayMenu() {
        for (Map.Entry<MenuOption, MenuItem> option : menu.entrySet()) {
            System.out.printf("%s - %s%n", option.getKey().getKeyToPress(),
                    option.getValue().getDescription());
        }

        MenuOption chosen;
        do {
            chosen = awaitValidMenuOption();
        } while (!chosen.equals(MenuOption.QUIT));
    }

    private MenuOption awaitValidMenuOption() {
        String choice;
        MenuOption chosen;
        do {
            choice = input.nextLine().toUpperCase().trim();
            chosen = MenuOption.get(choice);
        } while (chosen == null || !menu.containsKey(chosen));

        menu.get(chosen).getAction().execute();

        return chosen;
    }

    public String readOption() {
        return input.nextLine().trim().split("\\s", 1)[0];
    }

    public String readLine() {
        return input.nextLine();
    }

    public void addMenuItemAction(MenuOption key, MenuAction action) {
        MenuItem item = menu.get(key);
        item.linkAction(action);
        menu.put(key, item);
    }

    public void printMessage(String message) {
        System.out.println(message);
    }

    public void print(Serializable contents) {
        System.out.println(contents.toString());
    }
}
