package me.krawtschenko.yulia.model;

import java.io.IOException;
import java.nio.file.Path;

public class TextReader {
    public String read(Path path) throws IOException {
        StringBuilder stringToBe = new StringBuilder();
        String line;

        try (TextLoader loader = new TextLoader(path)) {

            while ((line = loader.readLine()) != null) {
                stringToBe.append(line);
            }
        }
        return stringToBe.toString();
    }
}
