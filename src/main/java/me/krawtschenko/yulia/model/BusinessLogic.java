package me.krawtschenko.yulia.model;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;

public class BusinessLogic {
    public String concatenateObjects(Object... objects) {
        return StringUtils.concatenate(objects);
    }

    public boolean matchAgainstSentenceRegEx(String string) {
        return RegExUtils.checkSentence(string);
    }

    public String[] splitOnTheOrYou(String string) {
        Pattern theOrYou = Pattern.compile("the|you");

        return RegExUtils.splitOnRegEx(string, theOrYou);
    }

    public String replaceVowelsWithUnderscores(String string) {
        return RegExUtils.vowelsToUnderscore(string);
    }

    public TreeMap<Character, TreeSet<Word>>
    groupWordsByFirstLetter(Text text) {
        return text.groupWordsByLetter();
    }

    public TreeSet<Word> sortWordsByVowelPercentage(Text text) {
        VowelPercentageWordComparator comparator
                = new VowelPercentageWordComparator();

        return text.sortWords(comparator);
    }

    public TreeSet<Word> sortVowelFirstByFirstConsonant(Text text) {
        WordFilter firstVowel = new WordFilter() {
            @Override
            public boolean filter(Word word) {
                return word.startsWithVowel();
            }
        };

        Set<Word> onlyVowelFirst = text.filterWords(firstVowel);

        HashMap<Word, Integer> words = new HashMap<>();
        onlyVowelFirst.forEach(word -> words.put(word, 1));
        Text newText = new Text(words);

        FirstConsonantWordComparator comparator
                = new FirstConsonantWordComparator();

        return newText.sortWords(comparator);
    }

    public Text readTextFromFile(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        TextReader reader = new TextReader();

        return new Text(reader.read(path));
    }
}
