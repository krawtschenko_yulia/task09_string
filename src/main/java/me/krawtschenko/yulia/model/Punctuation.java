package me.krawtschenko.yulia.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Punctuation {
    private String symbol;
    private boolean precededByBlank;
    private int comesAfterWordAt;

    public Punctuation(String wordSeparator, int comesAfterWordAt) {
        if (!"[\\W\\D\\S]".matches(wordSeparator)) {
            throw new IllegalArgumentException("Not a punctuation symbol.");
        }

        if ("[\\s]+(?=[\\W\\D\\S])".matches(wordSeparator)) {
            precededByBlank = true;
        }

        Pattern symbol = Pattern.compile("[\\W\\D\\S]");
        Matcher matcher = symbol.matcher(wordSeparator);

        int start = matcher.start();
        int end = matcher.end();

        this.symbol = wordSeparator.substring(start, end);
        this.comesAfterWordAt = comesAfterWordAt;
    }
}
