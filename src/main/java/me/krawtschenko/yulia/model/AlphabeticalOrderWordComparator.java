package me.krawtschenko.yulia.model;

import java.util.Comparator;

public class AlphabeticalOrderWordComparator implements Comparator<Word> {
    @Override
    public int compare(Word one, Word two) {
        return one.toString().compareTo(two.toString());
    }
}
