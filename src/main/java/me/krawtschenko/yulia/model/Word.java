package me.krawtschenko.yulia.model;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Word implements Comparable{
    private String string;

    public Word(String string) {
        string = string.trim().toLowerCase();
        if (string.matches("\\w+")) {
            this.string = string;
        } else {
            throw new IllegalArgumentException("Not a word: " + string);
        }
    }

    public static boolean isWord(String string) {
        return string.matches("/w+");
    }

    public boolean equals(Word other) {
        return string.equals(other.string);
    }

    public int getLength() {
        return string.length();
    }

    @Override
    public String toString() {
        return string;
    }

    public boolean startsWithSameLetter(Word other) {
        return string.charAt(0) == other.string.charAt(0);
    }

    public char getFirstLetter() {
        return string.charAt(0);
    }

    public boolean startsWithVowel() {
        return string.matches("^[aoeui].+");
    }

    public int getNumberVowels() {
        Pattern vowel = Pattern.compile("[aoeui]");

        return findMatches(vowel, string).size();
    }

    private ArrayList<String> findMatches(Pattern pattern, String input) {
        Matcher matcher = pattern.matcher(input);

        ArrayList<String> matches = new ArrayList<>();
        while (matcher.find()) {
            matches.add(matcher.group());
        }
        return matches;
    }

    public int compareTo(Word other) {
        return string.compareTo(other.string);
    }

    @Override
    public int compareTo(Object other) {
        if (!(other instanceof Word)) {
            throw new IllegalArgumentException("Not a word: " + string);
        }
        return string.compareTo(((Word) other).string);
    }
}
