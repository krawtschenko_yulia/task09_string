package me.krawtschenko.yulia.controller;

import me.krawtschenko.yulia.model.BusinessLogic;
import me.krawtschenko.yulia.model.Text;
import me.krawtschenko.yulia.view.ApplicationView;
import me.krawtschenko.yulia.view.MenuAction;
import me.krawtschenko.yulia.view.MenuOption;

import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApplicationController {
    private BusinessLogic model;
    private ApplicationView view;

    private MenuAction manipulateStrings;
    private MenuAction manipulateWords;
    private MenuAction manipulateSentences;
    private MenuAction manipulateText;

    private final Logger logger = LogManager.getLogger(ApplicationController.class);

    public ApplicationController() {
        try {
            model = new BusinessLogic();
            view = new ApplicationView();

            manipulateStrings = new MenuAction() {
                @Override
                public void execute() {
                    view.printMessage("CONCATENATING");
                    concatenate();

                    view.printMessage("MATCHING");
                    matchSentence();

                    view.printMessage("SPLITTING");
                    splitString();

                    view.printMessage("REPLACING");
                    vowelsToUnderscores();
                }
            };
            view.addMenuItemAction(MenuOption.REGEX_ON_STRINGS, manipulateStrings);

            manipulateWords = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        Text text = model.readTextFromFile("text.txt");

                        view.printMessage("GROUPED");
                        view.printGroupedWords(model.groupWordsByFirstLetter(text));

                        view.printMessage("SORTED");
                        view.printSet(model.sortWordsByVowelPercentage(text));

                        view.printMessage("FILTERED_SORTED");
                        view.printSet(model.sortVowelFirstByFirstConsonant(text));
                    } catch (IOException ex) {
                        logger.error(ex.getLocalizedMessage());
                    }
                }
            };
            view.addMenuItemAction(MenuOption.REGEX_ON_WORDS, manipulateWords);

            MenuAction quit = () -> System.exit(0);
            view.addMenuItemAction(MenuOption.QUIT, quit);
        } catch (RuntimeException ex) {
            logger.error(ex.getStackTrace());
        }
    }

    public void startMenu() {
        try {
            view.displayMenu();
        } catch (RuntimeException ex) {
            logger.error(ex.getStackTrace());
        }
    }

    private String formatObjectAndTypeString(Object object) {
        return String.format("%s (%s)", object.toString(),
                object.getClass().getName());
    }

    private void concatenate() {
        String string = "text in a string";
        int integer = 14;
        double trouble = 2.2;
        MenuAction action = () -> {
        };

        view.printMessage("INPUT");
        view.print(formatObjectAndTypeString(string));
        view.print(formatObjectAndTypeString(integer));
        view.print(formatObjectAndTypeString(trouble));
        view.print(formatObjectAndTypeString(action));

        String output = model.concatenateObjects(string, integer,
                trouble, action, this);

        view.printMessage("OUTPUT");
        view.print(output);
    }

    private void matchSentence() {
        view.printMessage("ENTER_SENTENCE");
        String sentence = view.readLine();
        boolean verdict = model.matchAgainstSentenceRegEx(sentence);
        view.print(verdict);
    }

    private void splitString() {
        view.printMessage("ENTER_TEXT");
        String input = view.readLine();
        String[] output = model.splitOnTheOrYou(input);
        view.printArray(output);
    }

    private void vowelsToUnderscores() {
        view.printMessage("ENTER_TEXT");
        String input = view.readLine();
        String output = model.replaceVowelsWithUnderscores(input);
        view.print(output);

    }
}