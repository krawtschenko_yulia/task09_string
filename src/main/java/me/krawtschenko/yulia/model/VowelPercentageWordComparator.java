package me.krawtschenko.yulia.model;

import java.util.Comparator;

public class VowelPercentageWordComparator implements Comparator<Word> {
    @Override
    public int compare(Word one, Word two) {
        double vowelPercentageInFirst = getVowelPercentage(one);
        double vowelPercentageInSecond = getVowelPercentage(two);

        return Double.compare(vowelPercentageInFirst, vowelPercentageInSecond);
    }

    private double getVowelPercentage(Word word) {
        return (double) word.getNumberVowels() / word.getLength();
    }
}
