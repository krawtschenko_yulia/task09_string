package me.krawtschenko.yulia.view;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public enum MenuOption {
    QUIT("Q"),
    REGEX_ON_STRINGS("1"),
    REGEX_ON_WORDS("2"),
    REGEX_ON_SENTENCES("3"),
    REGEX_ON_TEXT("4"),

    BACK_TO_MAIN_MENU("B");

    private final String key;

    MenuOption(String key) {
        this.key = key;
    }

    MenuOption() {
        key = "";
    }

    public String getKeyToPress() {
        return key;
    }

    private static final Map<String, MenuOption> table = new HashMap<>();

    static {
        for (MenuOption option : MenuOption.values()) {
            table.put(option.getKeyToPress(), option);
        }
    }

    public static MenuOption get(String key) {
        return table.get(key);
    }
}

class OptionKeyComparator implements Comparator<MenuOption> {
    @Override
    public int compare(MenuOption o1, MenuOption o2) {
        return o1.getKeyToPress().compareTo(o2.getKeyToPress());
    }
}
